package com.pai.gmtoolkit;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.pai.gmtoolkit.model.dh2edition.DH2ECharacter;

/**
 * Created by 1 on 01.03.2015.
 */
public class CharlistFragment extends Fragment {

    private ListView listView;
    private CharListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = View.inflate(getActivity(), R.layout.activity_char_list, null);
        listView = (ListView) rootView.findViewById(R.id.listView);
        adapter = new CharListAdapter(getActivity());
        adapter.addPlayerCharacter(new DH2ECharacter("Coraaal","Никич"));
        adapter.addPlayerCharacter(new DH2ECharacter("Ferratus Kek","Кек"));
        adapter.addPlayerCharacter(new DH2ECharacter("Felicia","Мишаня"));
        adapter.addPlayerCharacter(new DH2ECharacter("DIOMEDES","Артур"));

        adapter.addNonPlayerCharacter(new DH2ECharacter("Kaldor Draigo","NPC"));
        adapter.addNonPlayerCharacter(new DH2ECharacter("Varro Tigurius","NPC"));

        listView.setAdapter(adapter);
        return rootView;
    }
}
