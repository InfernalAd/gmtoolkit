package com.pai.gmtoolkit.customviews;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.pai.gmtoolkit.R;

/**
 * Created by 1 on 07.02.2015.
 */
public class Marker extends ImageView {
    private int dragPointerId = -1;
    float prevX = 0;
    float prevY = 0;
    private float posX = 0;
    private float posY = 0;

    public Marker(Context context) {
        super(context);

        init();
    }

    private void init() {
        setImageResource(R.drawable.marker);
    }

    public Marker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Marker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }



    @Override
    public void setOnTouchListener(OnTouchListener l) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        handleDrag(event);
        return true;
    }

    private boolean handleDrag(MotionEvent event) {
        if (event.getPointerCount() > 1) return false;
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
            if (dragPointerId == -1)
                dragPointerId = event.getPointerId(0);
            int pointerIndex = event.findPointerIndex(dragPointerId);
            if (pointerIndex >=0) {
                prevX = event.getX();
                prevY = event.getY();
            }
        }
        else if (event.getAction() == MotionEvent.ACTION_MOVE)
        {
            int pointerIndex = event.findPointerIndex(dragPointerId);
            if (pointerIndex >=0) {
                posX += (event.getX()-prevX);
                posY += (event.getY()-prevY);
                setTranslationX(posX);
                setTranslationY(posY);
            }
            invalidate();
        }
        else if (event.getAction() == MotionEvent.ACTION_UP)
        {
            dragPointerId = -1;
        }

        return true;
    }
}
