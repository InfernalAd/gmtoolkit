package com.pai.gmtoolkit.customviews;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;

/**
 * Created by 1 on 03.03.2015.
 */
public class ExpandableLinearLayout extends LinearLayout {
    private ScaleAnimation expandAnimation;
    private ScaleAnimation collapseAnimation;
    private boolean isExpanded = true;
    private boolean isAnimating = false;
    private long animationDuration = 300;


    public ExpandableLinearLayout(Context context) {
        super(context);
    }

    public ExpandableLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpandableLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ExpandableLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        init();
    }

    private void init() {
        expandAnimation = new ScaleAnimation(1,1,0,1);
        expandAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isAnimating = true;
                setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isAnimating = false;
                isExpanded = true;
                setVisibility(VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                isAnimating = true;
            }
        });
        collapseAnimation = new ScaleAnimation(1,1,1,0);
        collapseAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isAnimating = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isAnimating = false;
                isExpanded = false;
                setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                isAnimating = true;
            }
        });

        expandAnimation.setDuration(animationDuration);
        collapseAnimation.setDuration(animationDuration);
    }

    public void expand()
    {
        if (isAnimating) return;
        startAnimation(expandAnimation);
    }

    public void collapse()
    {
        if (isAnimating) return;
        startAnimation(collapseAnimation);
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public boolean isAnimating() {
        return isAnimating;
    }
}
