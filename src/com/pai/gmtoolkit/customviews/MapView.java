package com.pai.gmtoolkit.customviews;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.RelativeLayout;
import com.pai.gmtoolkit.R;

/**
 * Created by 1 on 18.02.2015.
 */
public class MapView extends RelativeLayout{
    private float mScaleFactor = 1;
    private float posX = 0;
    private float posY = 0;
    public float maxScaleFactor = 16.0f;
    public float minScaleFactor = 1.0f;
    private Bitmap bitmap;
    private Paint paint;

    public MapView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setOnTouchListener(new OnTouchListener() {
            private int dragPointerId = -1;
            float prevX = 0;
            float prevY = 0;

            private MotionEvent.PointerCoords firstScalePointerPos = null;
            private MotionEvent.PointerCoords secondScalePointerPos = null;
            private float initialSpan = -1;
            private PointF focusPoint = new PointF();
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("OnTouch",String.valueOf(event.getPointerCount()));
                handleScale(event);
                handleDrag(event);
                return true;
            }

            private boolean handleDrag(MotionEvent event) {
                if (event.getPointerCount() > 1) return false;
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    if (dragPointerId == -1)
                        dragPointerId = event.getPointerId(0);
                    int pointerIndex = event.findPointerIndex(dragPointerId);
                    if (pointerIndex >=0) {
                        prevX = event.getX();
                        prevY = event.getY();
                    }
                }
                else if (event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    int pointerIndex = event.findPointerIndex(dragPointerId);
                    if (pointerIndex >=0) {
                        posX += (event.getX()-prevX);
                        posY += (event.getY()-prevY);
                        setTranslationX(posX);
                        setTranslationY(posY);
                    }
                    invalidate();
                }
                else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    dragPointerId = -1;
                }

                return true;
            }

            private boolean handleScale(MotionEvent event) {
              if (event.getPointerCount() < 2) {
                  firstScalePointerPos = null;
                  secondScalePointerPos = null;
                  initialSpan = -1;
                  return true;
              }
              if (event.getPointerCount() > 2) return false;
              if (event.getAction() == MotionEvent.ACTION_MOVE)
               {
                   if (firstScalePointerPos == null || secondScalePointerPos == null)
                   {
                       firstScalePointerPos = new MotionEvent.PointerCoords();
                       secondScalePointerPos = new MotionEvent.PointerCoords();
                       event.getPointerCoords(0,firstScalePointerPos);
                       event.getPointerCoords(1,secondScalePointerPos);
                       focusPoint.set(Math.min(firstScalePointerPos.x, secondScalePointerPos.x) + Math.abs(firstScalePointerPos.x - secondScalePointerPos.x)/2,
                               Math.min(firstScalePointerPos.y, secondScalePointerPos.y) + Math.abs(firstScalePointerPos.y - secondScalePointerPos.y)/2);
                       Log.d("Focus",focusPoint.toString());

                   }
                   event.getPointerCoords(0,firstScalePointerPos);
                   event.getPointerCoords(1,secondScalePointerPos);
                   PointF spanVec = new PointF();
                   if (initialSpan < 0)
                   {
                       spanVec.set(firstScalePointerPos.x - secondScalePointerPos.x,firstScalePointerPos.y - secondScalePointerPos.y);
                       initialSpan = spanVec.length();
                   }
                   else
                   {
                       spanVec.set(firstScalePointerPos.x - secondScalePointerPos.x,firstScalePointerPos.y - secondScalePointerPos.y);
                       float currentSpan = spanVec.length();
                       Log.d("Span",String.format("%f %f",currentSpan,initialSpan));
                       scale(currentSpan / initialSpan, focusPoint.x, focusPoint.y);
                   }
                }
                else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    firstScalePointerPos = null;
                    secondScalePointerPos = null;
                }

                return true;
            }

        });
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
       init();
    }

    /*@Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint = new Paint();
        canvas.drawBitmap(bitmap,0,0, paint);
    }*/

    public void scale(float scaleFactor, float pivotX, float pivotY) {
        //if (scaleFactor > 2){return;}
        Log.d("Scale",String.valueOf(scaleFactor));
        mScaleFactor *= scaleFactor;
        mScaleFactor = Math.max(minScaleFactor, Math.min(maxScaleFactor, mScaleFactor));
        setTranslationX(0);
        setTranslationY(0);
        this.setPivotX(pivotX+posX/mScaleFactor);
        this.setPivotY(pivotY+posY/mScaleFactor);
        this.setScaleX(mScaleFactor);
        this.setScaleY(mScaleFactor);
        setTranslationX(posX);
        setTranslationY(posY);
       // this.invalidate();
    }

    public void restore() {
        mScaleFactor = 1;
        this.setScaleX(mScaleFactor);
        this.setScaleY(mScaleFactor);
        this.invalidate();
    }
}
