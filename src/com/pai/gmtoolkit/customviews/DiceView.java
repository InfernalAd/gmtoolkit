package com.pai.gmtoolkit.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.pai.gmtoolkit.tools.DiceRoller;

/**
 * Created by 1 on 01.03.2015.
 */
public class DiceView extends LinearLayout {
    private DiceRoller.DiceType diceType = DiceRoller.DiceType.D6;
    public DiceView(Context context) {
        super(context);
    }

    public DiceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DiceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public DiceRoller.DiceType getDiceType() {
        return diceType;
    }

    public void setDiceType(DiceRoller.DiceType diceType) {
        this.diceType = diceType;
        switch (diceType)
        {
            case D3:
                break;
            case D4:
                break;
            case D5:
                break;
            case D6:
                break;
            case D8:
                break;
            case D10:
                break;
            case D12:
                break;
            case D20:
                break;
            case D100:
                break;
        }
    }
}
