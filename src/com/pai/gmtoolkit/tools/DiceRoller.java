package com.pai.gmtoolkit.tools;

/**
 * Created by 1 on 07.02.2015.
 */
public class DiceRoller {
    public enum DiceType
    {
        D3,
        D4,
        D5,
        D6,
        D8,
        D10,
        D12,
        D20,
        D100
    }

    public static final DiceRoller sharedInstance = new DiceRoller();

    private DiceRoller() {
    }

    public int RollDice(DiceType type)
    {
        switch (type)
        {
            case D3:
                return RandomGenerator.sharedInstance.getRandomNumber(1,3);
            case D4:
                return RandomGenerator.sharedInstance.getRandomNumber(1,4);
            case D5:
                return RandomGenerator.sharedInstance.getRandomNumber(1,5);
            case D6:
                return RandomGenerator.sharedInstance.getRandomNumber(1,6);
            case D8:
                return RandomGenerator.sharedInstance.getRandomNumber(1,8);
            case D10:
                return RandomGenerator.sharedInstance.getRandomNumber(1,10);
            case D12:
                return RandomGenerator.sharedInstance.getRandomNumber(1,12);
            case D20:
                return RandomGenerator.sharedInstance.getRandomNumber(1,20);
            case D100:
                return RandomGenerator.sharedInstance.getRandomNumber(1,100);
        }
        return -1;
    }

    public int[] RollDices(DiceType[] types)
    {
        int[] res = new int[types.length];
        for (int i=0;i!=types.length;i++)
        {
            DiceType type = types[i];
            res[i] = RollDice(type);
        }
        return res;
    }

    public int[] RollDices(DiceType type,int number)
    {
        int[] res = new int[number];
        for (int i=0;i!=number;i++)
        {
            res[i] = RollDice(type);
        }
        return res;
    }
}
