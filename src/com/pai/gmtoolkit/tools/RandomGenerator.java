package com.pai.gmtoolkit.tools;

import java.util.Random;

/**
 * Created by 1 on 07.02.2015.
 */
public class RandomGenerator {
    public static final RandomGenerator sharedInstance = new RandomGenerator();
    private final Random random = new Random(System.currentTimeMillis());


    private RandomGenerator() {
    }

    public int getRandomNumber(int min,int max)
    {
        return random.nextInt(Math.max(max,min) - Math.min(max,min)+1)+min;
    }
}
