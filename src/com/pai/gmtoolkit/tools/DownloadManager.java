package com.pai.gmtoolkit.tools;

import android.content.Context;
import android.os.Environment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import org.apache.http.Header;

import java.io.*;
import java.nio.channels.FileChannel;

/**
 * Created by 1 on 04.03.2015.
 */
public class DownloadManager {
    public interface DownloadListener{
        public void onSuccess();
        public void onFailure(int errorCode,String message);
    }

    public static final DownloadManager downloadManager = new DownloadManager();
    private final AsyncHttpClient client;
    private static Context ctx;

    private DownloadManager() {
        client = new AsyncHttpClient();
    }

    public void downloadFile(String url, final String localPath, final DownloadListener listener)
    {
        client.get(url, new FileAsyncHttpResponseHandler(ctx) {
            @Override
            public void onFailure(int i, Header[] headers, Throwable throwable, File file) {
                if (listener != null)
                    listener.onFailure(i,throwable.getMessage());
            }

            @Override
            public void onSuccess(int i, Header[] headers, File file) {
                File destination = new File(ctx.getExternalFilesDir(null).getPath()+"/"+localPath);
                try {
                    copy(file,destination);
                    if (listener != null)
                        listener.onSuccess();
                } catch (IOException e) {
                    e.printStackTrace();
                    if (listener != null)
                        listener.onFailure(-1,e.getMessage());
                }
            }
        });
    }

    public void copy(File src, File dst) throws IOException {
        dst.getParentFile().mkdir();
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    public static void setCtx(Context ctx) {
        DownloadManager.ctx = ctx;
    }
}
