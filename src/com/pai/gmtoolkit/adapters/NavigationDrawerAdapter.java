package com.pai.gmtoolkit.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.pai.gmtoolkit.R;
import org.w3c.dom.Text;

/**
 * Created by 1 on 01.03.2015.
 */
public class NavigationDrawerAdapter extends BaseAdapter {
    private final Context ctx;
    String[] drawerElements;

    public NavigationDrawerAdapter(Context ctx) {
        drawerElements = ctx.getResources().getStringArray(R.array.drawerElements);
        this.ctx = ctx;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return drawerElements.length;
    }

    @Override
    public Object getItem(int position) {
        return drawerElements[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View drawerElement;
        TextView textView;
        if (convertView == null) {
            drawerElement = View.inflate(ctx, R.layout.drawer_element, null);
            textView = (TextView) drawerElement.findViewById(R.id.textView);
        } else {
            drawerElement = convertView;
            textView = (TextView) drawerElement.findViewById(R.id.textView);
        }
        textView.setText(drawerElements[position]);
        return drawerElement;
    }
}
