package com.pai.gmtoolkit.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 04.03.2015.
 */
public class GameSystemsSpinnerAdapter extends BaseAdapter {
    private final Context ctx;
    private final List<String> systems = new ArrayList<String>();

    public GameSystemsSpinnerAdapter(Context ctx) {
        this.ctx = ctx;
        systems.add("DH2E");
    }


    @Override
    public int getCount() {
        return systems.size();
    }

    @Override
    public Object getItem(int position) {
        return systems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View element;
        if (convertView != null)
            element = convertView;
        else
            element = View.inflate(ctx, R.layout.element_gamesystem_list,null);
        TextView tv = (TextView) element.findViewById(R.id.textView);
        tv.setText((String) getItem(position));
        return element;
    }
}
