package com.pai.gmtoolkit.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.BoringLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETalent;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETrait;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 06.03.2015.
 */
public class TraitListAdapter extends BaseAdapter{
    private List<DH2ETalent> talents = new ArrayList<DH2ETalent>();
    private List<DH2ETrait> traits = new ArrayList<DH2ETrait>();
    private final Context ctx;

    public TraitListAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void addTalent(DH2ETalent talent)
    {
        talents.add(talent);
    }

    public void addTrait(DH2ETrait trait)
    {
        traits.add(trait);
    }

    @Override
    public int getCount() {
        return talents.size()+traits.size()+2;
    }

    @Override
    public Object getItem(int position) {
        if (position > 0 && position < talents.size()+1)
        {
            return talents.get(position-1);
        }
        if (position > talents.size()+2)
        {
            return traits.get(position-2);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView != null)
            view = convertView;
        else
            view = View.inflate(ctx, R.layout.element_trait_list,parent);
        TextView textView = (TextView) view.findViewById(R.id.textView);
        if (position == 0)
        {
            textView.setText("Talents");
            textView.setTextSize(40);
            textView.setTypeface(null, Typeface.BOLD);
        } else if (position < talents.size() +1)
        {
            textView.setText(talents.get(position-1).getName());
        }
        else if (position == talents.size() + 1)
        {
            textView.setText("Traits");
            textView.setTextSize(40);
            textView.setTypeface(null, Typeface.BOLD);
        }
        else
        {
            textView.setText(traits.get(position-traits.size()-2).getName());
        }

        return view;
    }
}
