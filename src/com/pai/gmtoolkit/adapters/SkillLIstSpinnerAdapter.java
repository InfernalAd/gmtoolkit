package com.pai.gmtoolkit.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ESkill;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 04.03.2015.
 */
public class SkillLIstSpinnerAdapter extends BaseAdapter {
    private Context ctx;
    private List<DH2ESkill> skillCodes = new ArrayList<DH2ESkill>();

    public SkillLIstSpinnerAdapter(Context ctx) {
        this.ctx = ctx;
        //this.skillCodes.addAll(DH2ESkill.skillNames.keySet());
    }

    @Override
    public int getCount() {
        return skillCodes.size();
    }

    @Override
    public Object getItem(int position) {
        return skillCodes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View element;
        if (convertView == null)
            element = View.inflate(ctx, R.layout.skill_list_spinner_elem,null);
        else
            element = convertView;
        TextView textView = (TextView) element.findViewById(R.id.skill_name_label);
        //textView.setText(DH2ESkill.skillNames.get((DH2ESkillCode)getItem(position)));
        return element;
    }
}
