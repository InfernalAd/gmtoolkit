package com.pai.gmtoolkit.adapters;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ESkill;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 04.03.2015.
 */
public class SkillListAdapter extends BaseAdapter {
    public static class SkillInfo
    {
        public DH2ESkill skill;
        public int level;

        public SkillInfo(DH2ESkill skill, int level) {
            this.skill = skill;
            this.level = level;
        }
    }
    private List<SkillInfo> skills = new ArrayList<SkillInfo>();
    private final Context ctx;

    public SkillListAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void add(SkillInfo skillInfo)
    {
        skills.add(skillInfo);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return skills.size();
    }

    @Override
    public Object getItem(int position) {
        return skills.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View skillView;
        if (convertView != null)
            skillView = convertView;
        else
            skillView = View.inflate(ctx, R.layout.element_skill_list,null);

        SkillInfo skillInfo = (SkillInfo) getItem(position);
        TextView skillName = (TextView) skillView.findViewById(R.id.skill_name_label);
        RatingBar skillLevel = (RatingBar) skillView.findViewById(R.id.skill_level_rating_bar);
       /* skillLevel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });*/
        skillName.setText(skillInfo.skill.getName());
        skillLevel.setRating(skillInfo.level);

        return skillView;
    }
}
