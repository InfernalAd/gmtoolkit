package com.pai.gmtoolkit.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.model.dh2edition.DH2ECampaign;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 04.03.2015.
 */
public class CampaignListAdapter extends BaseAdapter {
    private List<DH2ECampaign> campaigns = new ArrayList<DH2ECampaign>();
    private final Context ctx;

    public CampaignListAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return campaigns.size();
    }

    @Override
    public Object getItem(int position) {
        return campaigns.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View element;
        if (convertView == null)
            element = View.inflate(ctx, R.layout.element_campaign_list,null);
        else
            element = convertView;
        ImageView readyImageView = (ImageView) element.findViewById(R.id.image_ready);
        TextView textView = (TextView)element.findViewById(R.id.textView);
        DH2ECampaign campaignInfo = (DH2ECampaign) getItem(position);
        if (campaignInfo.isDownloaded())
            readyImageView.setVisibility(View.VISIBLE);
        else
            readyImageView.setVisibility(View.INVISIBLE);

        if (campaignInfo.isSelected())
            element.setBackgroundColor(Color.rgb(72, 72, 72));
        else
            element.setBackgroundColor(Color.rgb(48,48,48));

        textView.setText(campaignInfo.getName());
        return element;
    }

    public void add(DH2ECampaign campaign) {
        campaigns.add(campaign);
        notifyDataSetChanged();
    }
}
