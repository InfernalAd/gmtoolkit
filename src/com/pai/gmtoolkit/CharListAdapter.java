package com.pai.gmtoolkit;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.pai.gmtoolkit.model.Character;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by 1 on 01.03.2015.
 */
public class CharListAdapter extends BaseAdapter {

    private final Context ctx;
    private List<Character> playerCharacters = new ArrayList<Character>();
    private List<Character> nonPlayerCharacters = new ArrayList<Character>();

    public CharListAdapter(Context ctx) {
        this.ctx = ctx;
    }

    public void addPlayerCharacter(Character character) {
        playerCharacters.add(character);
    }

    public void addPlayerCharacters(Collection<? extends Character> character) {
        playerCharacters.addAll(character);
    }

    public void addNonPlayerCharacter(Character character) {
        nonPlayerCharacters.add(character);
    }

    public void addNonPlayerCharacters(Collection<? extends Character> character) {
        nonPlayerCharacters.addAll(character);
    }

    @Override
    public int getCount() {
        return playerCharacters.size() + nonPlayerCharacters.size() + 2;//2 additional slots for list separators
    }

    @Override
    public Object getItem(int position) {
        if (position == 0) {//First separator
            return null;
        } else if (position <= playerCharacters.size())//Player characters
        {
            return playerCharacters.get(position - 1);
        } else if (position == playerCharacters.size()+1) {//Second separator
            return null;
        } else {//Non player characters
            return nonPlayerCharacters.get(position - playerCharacters.size() - 1);
        }


    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0) {//First separator
            TextView separatorView = (TextView) View.inflate(ctx, R.layout.char_list_separator, null);
            separatorView.setText(ctx.getString(R.string.player_characters_label));
            return separatorView;
        } else if (position <= playerCharacters.size())//Player characters
        {
            View elementView = View.inflate(ctx, R.layout.char_list_elem, null);
            TextView name = (TextView) elementView.findViewById(R.id.char_name);
            TextView summary = (TextView) elementView.findViewById(R.id.char_summary);
            TextView playerName = (TextView) elementView.findViewById(R.id.player_name);
            TextView notes = (TextView) elementView.findViewById(R.id.char_notes);

            name.setText(playerCharacters.get(position - 1).getName());
            summary.setText(playerCharacters.get(position - 1).getSummary());
            playerName.setText(playerCharacters.get(position - 1).getPlayerName());
            notes.setText(playerCharacters.get(position - 1).getNotes());
            return elementView;
        } else if (position == playerCharacters.size()+1) {//Second separator
            TextView separatorView = (TextView) View.inflate(ctx, R.layout.char_list_separator, null);
            separatorView.setText(ctx.getString(R.string.non_player_characters_label));
            return separatorView;
        } else {//Non player characters
            View elementView = View.inflate(ctx, R.layout.char_list_elem, null);
            TextView name = (TextView) elementView.findViewById(R.id.char_name);
            TextView summary = (TextView) elementView.findViewById(R.id.char_summary);
            TextView playerName = (TextView) elementView.findViewById(R.id.player_name);
            TextView notes = (TextView) elementView.findViewById(R.id.char_notes);

            name.setText(nonPlayerCharacters.get(position - playerCharacters.size() - 2).getName());
            summary.setText(nonPlayerCharacters.get(position - playerCharacters.size() - 2).getSummary());
            playerName.setText(nonPlayerCharacters.get(position - playerCharacters.size() - 2).getPlayerName());
            notes.setText(nonPlayerCharacters.get(position - playerCharacters.size() - 2).getNotes());
            return elementView;
        }
    }
}
