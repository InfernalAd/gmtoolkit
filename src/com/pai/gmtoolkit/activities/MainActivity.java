package com.pai.gmtoolkit.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.*;
import com.google.gson.Gson;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.adapters.CampaignListAdapter;
import com.pai.gmtoolkit.adapters.GameSystemsSpinnerAdapter;
import com.pai.gmtoolkit.model.dh2edition.DH2ECampaign;
import com.pai.gmtoolkit.tools.DownloadManager;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    public final static String SERVER_URL = "https://s3.eu-central-1.amazonaws.com/gmtkdh";
    @ViewById(R.id.campaign_list)
    ListView campListView;
    @ViewById(R.id.game_systems_spinner)
    Spinner gameSystemSpinner;

    private ProgressDialog progressDialog;
    private CampaignListAdapter campaignAdapter;

    private DH2ECampaign[] campaigns;


    /**
     * Called when the activity is first created.
     */
    @AfterViews
    public void afterViews(){
        campaignAdapter = new CampaignListAdapter(this);
        campListView.setAdapter(campaignAdapter);
        campListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onCampaignClicked(position);
            }
        });
        gameSystemSpinner.setAdapter(new GameSystemsSpinnerAdapter(this));


        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        final Context ctx = this;
        DownloadManager.setCtx(getBaseContext());
        DownloadManager.downloadManager.downloadFile(SERVER_URL + "/info.json", "info.json", new DownloadManager.DownloadListener() {
            @Override
            public void onSuccess() {
                progressDialog.dismiss();
                onInfoJsonLoaded();
            }

            @Override
            public void onFailure(int errorCode, String message) {
                progressDialog.dismiss();
                new AlertDialog.Builder(ctx).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setTitle("Connectivity error").setMessage("It looks like I cannot reach server. Please, check your internet connection.").show();
            }
        });
    }

    private void onCampaignClicked(int position) {
        for (DH2ECampaign campaign:campaigns)
            campaign.setSelected(false);
        campaigns[position].setSelected(true);
        DH2ECampaign.selectedCampaignId = campaigns[position].getId();
        DH2ECampaign.selectedCampaign = campaigns[position];
        campaignAdapter.notifyDataSetChanged();
        if (!campaigns[position].isDownloaded()) {
            progressDialog.show();
            downloadCampaign(campaigns[position]);
        }
    }

    private void downloadCampaign(final DH2ECampaign campaign) {
        DownloadManager.DownloadListener campaignDownloadListener = new DownloadManager.DownloadListener() {
            private int filesDownloaded = 0;
            private int campaignId = campaign.getId();
            @Override
            public void onSuccess() {
                filesDownloaded++;
                if (filesDownloaded >= 5)
                    onCampaignLoaded(campaignId);
            }

            @Override
            public void onFailure(int errorCode, String message) {
                onCampaignLoadFailed(campaignId,message);
            }

        };
        DownloadManager.downloadManager.downloadFile(SERVER_URL + "/" + campaign.getDirectoryPath() + "characters.json", campaign.getDirectoryPath() + "characters.json", campaignDownloadListener);
        DownloadManager.downloadManager.downloadFile(SERVER_URL + "/" + campaign.getDirectoryPath() + "skills.json", campaign.getDirectoryPath() + "skills.json", campaignDownloadListener);
        DownloadManager.downloadManager.downloadFile(SERVER_URL + "/" + campaign.getDirectoryPath() + "talents.json", campaign.getDirectoryPath() + "talents.json", campaignDownloadListener);
        DownloadManager.downloadManager.downloadFile(SERVER_URL + "/" + campaign.getDirectoryPath() + "traits.json", campaign.getDirectoryPath() + "traits.json", campaignDownloadListener);
        DownloadManager.downloadManager.downloadFile(SERVER_URL + "/" + campaign.getDirectoryPath() + "items.json", campaign.getDirectoryPath() + "items.json", campaignDownloadListener);
    }

    private void onCampaignLoadFailed(int campaignId,String error) {
        new AlertDialog.Builder(this).setMessage(String.format("Campaign %s failed to load. Error:%s",campaigns[campaignId].getName(),error)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
        progressDialog.dismiss();
    }

    private void onCampaignLoaded(int campaignId) {
        campaigns[campaignId].setDownloaded(true);
        campaignAdapter.notifyDataSetChanged();
        progressDialog.dismiss();
        if (campaigns[campaignId].isDownloaded())
            campaigns[campaignId].loadFromJson();

    }

    private boolean isCampaignDownloaded(DH2ECampaign infocampaign) {
        File characters = new File(getExternalFilesDir(null)+"/"+ infocampaign.getDirectoryPath() + "characters.json");
        File skills = new File(getExternalFilesDir(null)+"/"+ infocampaign.getDirectoryPath() + "skills.json");
        File talents = new File(getExternalFilesDir(null)+"/"+ infocampaign.getDirectoryPath() + "talents.json");
        File traits = new File(getExternalFilesDir(null)+"/"+ infocampaign.getDirectoryPath() + "traits.json");
        File items = new File(getExternalFilesDir(null)+"/"+ infocampaign.getDirectoryPath() + "items.json");
        return characters.exists() && skills.exists() && talents.exists() && traits.exists() && items.exists();
    }

    private void onInfoJsonLoaded() {
        try {
            FileReader fileReader = new FileReader(new File(getExternalFilesDir(null).getPath()+"/info.json"));
            Gson gson = new Gson();
            campaigns = gson.fromJson(fileReader, DH2ECampaign[].class);
            for (DH2ECampaign camp : campaigns)
                campaignAdapter.add(camp);
            if (DH2ECampaign.selectedCampaignId != -1)
                campaigns[DH2ECampaign.selectedCampaignId].setSelected(true);
            for (DH2ECampaign campaign:campaigns)
            {
                campaign.setCtx(getBaseContext());
                campaign.setDownloaded((isCampaignDownloaded(campaign)));
                if (campaign.isDownloaded())
                    campaign.loadFromJson();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}
