package com.pai.gmtoolkit.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.*;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.adapters.SkillLIstSpinnerAdapter;
import com.pai.gmtoolkit.adapters.SkillListAdapter;
import com.pai.gmtoolkit.adapters.TraitListAdapter;
import com.pai.gmtoolkit.customviews.ExpandableLinearLayout;
import com.pai.gmtoolkit.model.dh2edition.DH2ECharacter;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ESkill;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETalent;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETrait;
import org.androidannotations.annotations.*;

/**
 * Created by 1 on 03.03.2015.
 */
@EActivity(R.layout.activity_char_list_view)
public class CharActivity extends BaseActivity {
    @ViewById(R.id.mainParametersExpandLayout)
    ExpandableLinearLayout mainParametersLayout;
    @ViewById(R.id.characteristicsLayout)
    ExpandableLinearLayout characteristicsLayout;
    @ViewById(R.id.skillsExpandLayout)
    ExpandableLinearLayout skillsExpandLayout;
    @ViewById(R.id.talentsAndTraitsExpandLayout)
    ExpandableLinearLayout talentsAndTraitsLayout;

    @ViewById(R.id.skill_list_layout)
    LinearLayout skillListLayout;
    @ViewById(R.id.talents_and_traits_list_layout)
    LinearLayout traitsListLayout;

    //<editor-fold desc="main parameters>
    @ViewById(R.id.characterNameEditText)
    TextView charNameEditText;
    @ViewById(R.id.playerNameEditText)
    TextView playerNameEditText;
    @ViewById(R.id.homeworldEditTExt)
    TextView homeworldEditText;
    @ViewById(R.id.backgroundEditText)
    TextView backgroundEditText;
    @ViewById(R.id.roleEditText)
    TextView roleEditText;
    @ViewById(R.id.eliteAdvancesEditText)
    TextView eliteAdvancesEditText;
    @ViewById(R.id.divinationEditText)
    TextView divinationEditText;
    @ViewById(R.id.corruption_editText)
    TextView corruptionEditText;
    @ViewById(R.id.insanity_EditText)
    TextView insanityEditText;
    @ViewById(R.id.notesEditText)
    TextView notesEditText;
    //</editor-fold>


    //<editor-fold desc="characteristics>
    @ViewById(R.id.ws_editText)
    TextView weaponSkillEditText;
    @ViewById(R.id.bs_editText)
    TextView ballisticSkillEditText;
    @ViewById(R.id.strength_editText)
    TextView strengthEditText;
    @ViewById(R.id.toughness_editText)
    TextView toughnessEditText;
    @ViewById(R.id.agility_editText)
    TextView agilityEditText;
    @ViewById(R.id.intelligence_editText)
    TextView intelligenceEditText;
    @ViewById(R.id.perception_edit_text)
    TextView perceptionEditText;
    @ViewById(R.id.willpower_editText)
    TextView willpowerEditText;
    @ViewById(R.id.fellowship_editText)
    TextView fellowshipEditText;
    @ViewById(R.id.influence_editText)
    TextView influenceEditText;
    //</editor-fold>

    @Extra("character")
    DH2ECharacter currentCharacter;

    private SkillListAdapter skillListAdapter = new SkillListAdapter(this);
    private TraitListAdapter traitListAdapter = new TraitListAdapter(this);

    @AfterViews
    public void afterViews()
    {
        initMainParameters();
        initSkills();
        initTalentsAndTraits();
        initGear();

    }

    private void initGear() {

    }

    private void initTalentsAndTraits() {
        for (DH2ETalent talent: currentCharacter.getTalents())
        {
            traitListAdapter.addTalent(talent);
        }

        for (DH2ETrait trait: currentCharacter.getTraits())
        {
            traitListAdapter.addTrait(trait);
        }

        for (int i=0;i!=traitListAdapter.getCount();i++)
        {
            traitsListLayout.addView(traitListAdapter.getView(i,null,null));
        }
    }

    private void initSkills() {
        for (DH2ESkill skill: currentCharacter.getSkillMap().keySet())
        {
            SkillListAdapter.SkillInfo skillInfo = new SkillListAdapter.SkillInfo(skill, currentCharacter.getSkillMap().get(skill));
            skillListAdapter.add(skillInfo);
        }
        for (int i=0;i!=skillListAdapter.getCount();i++)
        {
            skillListLayout.addView(skillListAdapter.getView(i,null,skillListLayout));
        }
    }

    private void initMainParameters() {
        charNameEditText.setText(currentCharacter.getName());
        playerNameEditText.setText(currentCharacter.getPlayerName());
        homeworldEditText.setText(currentCharacter.getHomeWorld());
        backgroundEditText.setText(currentCharacter.getBackground());
        roleEditText.setText(currentCharacter.getRole());
        eliteAdvancesEditText.setText(currentCharacter.getEliteAdvances());
        //divinationEditText.setText(currentCharacter.get());
        corruptionEditText.setText(String.valueOf(currentCharacter.getCorruption()));
        insanityEditText.setText(String.valueOf(currentCharacter.getInsanity()));
        notesEditText.setText(currentCharacter.getNotes());

        weaponSkillEditText.setText(String.valueOf(currentCharacter.getWeaponSkill()));
        ballisticSkillEditText.setText(String.valueOf(currentCharacter.getBallisticSkill()));
        strengthEditText.setText(String.valueOf(currentCharacter.getStrength()));
        toughnessEditText.setText(String.valueOf(currentCharacter.getToughness()));
        agilityEditText.setText(String.valueOf(currentCharacter.getAgility()));
        intelligenceEditText.setText(String.valueOf(currentCharacter.getIntelligence()));
        perceptionEditText.setText(String.valueOf(currentCharacter.getPerception()));
        willpowerEditText.setText(String.valueOf(currentCharacter.getWillpower()));
        fellowshipEditText.setText(String.valueOf(currentCharacter.getFellowship()));
        influenceEditText.setText(String.valueOf(currentCharacter.getInfluence()));
    }


    @Click(R.id.mainParametersExpandButton)
    public void expandMainLayout()
    {
        if (mainParametersLayout.isExpanded())
            mainParametersLayout.collapse();
        else
            mainParametersLayout.expand();
    }
    @Click(R.id.characteristicsExpandButton)
    public void expandCharacteristicsLayout()
    {
        if (characteristicsLayout.isExpanded())
            characteristicsLayout.collapse();
        else
            characteristicsLayout.expand();
    }

    @Click(R.id.skillsExpandButton)
     public void expandSkillsLayout()
    {
        if (skillsExpandLayout.isExpanded())
            skillsExpandLayout.collapse();
        else
            skillsExpandLayout.expand();
    }

    @Click(R.id.talentsExpandButton)
    public void expandTalentsLayout()
    {
        if (talentsAndTraitsLayout.isExpanded())
            talentsAndTraitsLayout.collapse();
        else
            talentsAndTraitsLayout.expand();
    }

    @Click(R.id.addSkillButton)
    public void addSkill()
    {
        //DH2ESkill.initSkillNames(this);
        AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View inflatedView = View.inflate(this, R.layout.popup_new_skill, null);

        final Spinner skillSpinner = (Spinner) inflatedView.findViewById(R.id.skill_spinner);
        skillSpinner.setAdapter(new SkillLIstSpinnerAdapter(this));
        final RatingBar levelRatingBar = (RatingBar) inflatedView.findViewById(R.id.skill_level_rating_bar);
        levelRatingBar.setRating(0);
        levelRatingBar.setNumStars(4);

        builder.setView(inflatedView);
        final Context ctx = this;
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //DH2ESkillCode skillCode = (DH2ESkillCode) skillSpinner.getAdapter().getItem(skillSpinner.getSelectedItemPosition());
               // DH2ESkill skillId = DH2ESkill.createInstance(ctx, skillCode,(int)levelRatingBar.getRating());
                //skillListAdapter.add(skillId);
            }
        });





        dialog = builder.create();
        dialog.show();
    }

}
