package com.pai.gmtoolkit.activities;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.pai.gmtoolkit.CharListAdapter;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.model.dh2edition.DH2ECampaign;
import com.pai.gmtoolkit.model.dh2edition.DH2ECharacter;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;

/**
 * Created by 1 on 01.03.2015.
 */
@EActivity(R.layout.activity_char_list)
public class CharListActivity extends BaseActivity {
    @ViewById(R.id.listView)
    ListView listView;
    private CharListAdapter adapter;

    @AfterViews
    public void afterViews()
    {
        adapter = new CharListAdapter(this);
        if (DH2ECampaign.selectedCampaign == null) return;
        for (DH2ECharacter playerCharacter:DH2ECampaign.selectedCampaign.getCharacters().getPlayerCharacters())
        {
            adapter.addPlayerCharacter(playerCharacter);
        }

        for (DH2ECharacter nonPlayerCharacter:DH2ECampaign.selectedCampaign.getCharacters().getNonPlayerCharacters())
        {
            adapter.addNonPlayerCharacter(nonPlayerCharacter);
        }

        listView.setAdapter(adapter);
        final Context ctx = this;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CharActivity_.intent(ctx).currentCharacter((DH2ECharacter) adapter.getItem(position)).start();
            }
        });
    }

}
