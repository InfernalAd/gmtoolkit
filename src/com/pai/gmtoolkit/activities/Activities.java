package com.pai.gmtoolkit.activities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 1 on 01.03.2015.
 */
public class Activities {
    public final static Activities instance = new Activities();
    private final Map<Integer,Class<?>> activityClasses = new HashMap<Integer,Class<?>>();

    private Activities() {
        activityClasses.put(0,MainActivity_.class);
        activityClasses.put(1,CharListActivity_.class);
        activityClasses.put(2,DicerollerActivity_.class);
        activityClasses.put(3,MapViewerActivity_.class);
    }

    public Class<?> get(int id)
    {
        return activityClasses.get(id);
    }
}
