package com.pai.gmtoolkit.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.pai.gmtoolkit.R;
import com.pai.gmtoolkit.adapters.NavigationDrawerAdapter;

/**
 * Created by 1 on 01.03.2015.
 */
public class BaseActivity extends Activity {
    protected static int currentDrawerPosition = 0;
    int[] drawerClassIds;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onStart() {
        super.onStart();
        initNavDrawer();
    }

    protected void initNavDrawer() {
        drawerClassIds = getResources().getIntArray(R.array.drawerClassIds);
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ListView navigationDrawerList = (ListView) findViewById(R.id.navigation_drawer);
        navigationDrawerList.setAdapter(new NavigationDrawerAdapter(this));
        navigationDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onDrawerElementClicked(position);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    private void onDrawerElementClicked(int position) {
        if (position == currentDrawerPosition) return;
        currentDrawerPosition = position;
        int id = drawerClassIds[position];
        Class<?> activityClass = Activities.instance.get(id);
        if (activityClass != null)
        {
            Intent intent = new Intent(this, activityClass);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }
}
