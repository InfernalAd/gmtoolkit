package com.pai.gmtoolkit.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 1 on 01.03.2015.
 */
public abstract class Character implements Serializable {
    @SerializedName("name")
    protected final String name;
    @SerializedName("player_name")
    protected final String playerName;
    @SerializedName("notes")
    protected String notes;

    protected Character(String name, String playerName) {
        this.name = name;
        this.playerName = playerName;
    }

    public String getName() {
        return name;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public abstract String getSummary() ;

}
