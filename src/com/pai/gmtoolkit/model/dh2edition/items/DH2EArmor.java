package com.pai.gmtoolkit.model.dh2edition.items;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1 on 05.03.2015.
 */
public class DH2EArmor extends DH2EItem{
    @SerializedName("locations_covered")
    private String locationsCovered;
    @SerializedName("armor_points")
    private int armorPoints;
    @SerializedName("max_agility")
    private int maxAgility;

    public String getLocationsCovered() {
        return locationsCovered;
    }

    public int getArmorPoints() {
        return armorPoints;
    }

    public int getMaxAgility() {
        return maxAgility;
    }
}
