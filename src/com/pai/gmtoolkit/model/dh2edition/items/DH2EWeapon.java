package com.pai.gmtoolkit.model.dh2edition.items;

import com.google.gson.annotations.SerializedName;
import com.pai.gmtoolkit.model.dh2edition.items.DH2EItem;

/**
 * Created by 1 on 05.03.2015.
 */
public class DH2EWeapon extends DH2EItem {
    @SerializedName("type")
    private String type;
    @SerializedName("damage")
    private String damageFormula;


    public String getType() {
        return type;
    }

    public String getDamageFormula() {
        return damageFormula;
    }

}
