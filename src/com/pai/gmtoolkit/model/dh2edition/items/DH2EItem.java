package com.pai.gmtoolkit.model.dh2edition.items;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1 on 05.03.2015.
 */
public class DH2EItem {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("weigth")
    private float weight;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public float getWeight() {
        return weight;
    }
}
