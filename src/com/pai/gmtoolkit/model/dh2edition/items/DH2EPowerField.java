package com.pai.gmtoolkit.model.dh2edition.items;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 1 on 05.03.2015.
 */
public class DH2EPowerField extends DH2EItem {
    @SerializedName("protection_rating")
    private int protectionRating;
    @SerializedName("overload_chance")
    private float overloadChance;

    public int getProtectionRating() {
        return protectionRating;
    }

    public float getOverloadChance() {
        return overloadChance;
    }
}
