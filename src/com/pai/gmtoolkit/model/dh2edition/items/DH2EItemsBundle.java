package com.pai.gmtoolkit.model.dh2edition.items;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by 1 on 05.03.2015.
 */
public class DH2EItemsBundle {
    @SerializedName("weapons")
    DH2EWeapon[] weaponsList;
    @SerializedName("armors")
    DH2EArmor[] armorList;
    @SerializedName("power_fields")
    DH2EPowerField[] powerFieldsList;
    @SerializedName("gear")
    DH2EItem[] gearList;

    Map<Integer,DH2EWeapon> weaponsMap = new HashMap<Integer, DH2EWeapon>();
    Map<Integer,DH2EArmor> armorMap = new HashMap<Integer, DH2EArmor>();
    Map<Integer,DH2EPowerField> powerFieldsMap = new HashMap<Integer, DH2EPowerField>();
    Map<Integer,DH2EItem> gearMap = new HashMap<Integer, DH2EItem>();


    public void initHashMaps()
    {
       for (DH2EWeapon weapon:weaponsList)
           weaponsMap.put(weapon.getId(),weapon);

        for (DH2EArmor weapon:armorList)
            armorMap.put(weapon.getId(),weapon);

        for (DH2EPowerField weapon:powerFieldsList)
            powerFieldsMap.put(weapon.getId(),weapon);

        for (DH2EItem item:gearList)
            gearMap.put(item.getId(), item);
    }

    public Set<Integer> getWeaponKeys()
    {
        if (weaponsMap.size() != weaponsList.length) initHashMaps();
        return weaponsMap.keySet();
    }
    public DH2EWeapon getWeapon(int id)
    {
        if (weaponsMap.size() != weaponsList.length) initHashMaps();
        return weaponsMap.get(id);
    }

    public Set<Integer> getArmorKeys()
    {
        if (armorMap.size() != armorList.length) initHashMaps();
        return armorMap.keySet();
    }
    public DH2EArmor getArmor(int id)
    {
        if (armorMap.size() != armorList.length) initHashMaps();
        return armorMap.get(id);
    }

    public Set<Integer> getPowerFieldKeys()
    {
        if (powerFieldsMap.size() != powerFieldsList.length) initHashMaps();
        return powerFieldsMap.keySet();
    }
    public DH2EPowerField getPowerField(int id)
    {
        if (powerFieldsMap.size() != powerFieldsList.length) initHashMaps();
        return powerFieldsMap.get(id);
    }

    public Set<Integer> getGearKeys()
    {
        if (gearMap.size() != gearList.length) initHashMaps();
        return gearMap.keySet();
    }
    public DH2EItem getGearItem(int id)
    {
        if (gearMap.size() != gearList.length) initHashMaps();
        return gearMap.get(id);
    }
}
