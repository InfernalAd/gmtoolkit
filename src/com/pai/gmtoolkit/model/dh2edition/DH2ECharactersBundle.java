package com.pai.gmtoolkit.model.dh2edition;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 05.03.2015.
 */
public class DH2ECharactersBundle {
    @SerializedName("player_characters")
    private List<DH2ECharacter> playerCharacters = new ArrayList<DH2ECharacter>();
    @SerializedName("non_player_characters")
    private List<DH2ECharacter> nonPlayerCharacters = new ArrayList<DH2ECharacter>();

    public List<DH2ECharacter> getPlayerCharacters() {
        //Defensive copies FTW
        return new ArrayList<DH2ECharacter>(playerCharacters);
    }

    public List<DH2ECharacter> getNonPlayerCharacters() {
        //Defensive copies FTW
        return new ArrayList<DH2ECharacter>(nonPlayerCharacters);
    }
}
