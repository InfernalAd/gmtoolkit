package com.pai.gmtoolkit.model.dh2edition;

import com.google.gson.annotations.SerializedName;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ESkill;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETalent;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETrait;
import com.pai.gmtoolkit.model.dh2edition.items.*;

import java.io.Serializable;
import java.util.*;

/**
 * Created by 1 on 01.03.2015.
 */
public class DH2ECharacter extends com.pai.gmtoolkit.model.Character implements Serializable {
    private class SkillInfo implements Serializable
    {
        @SerializedName("skillId")
        public int skillId;
        @SerializedName("level")
        public int level;
    }
    @SerializedName("weapon_skill")
    private int weaponSkill;
    @SerializedName("ballistic_skill")
    private int ballisticSkill;
    @SerializedName("strength")
    private int strength;
    @SerializedName("toughness")
    private int toughness;
    @SerializedName("agility")
    private int agility;
    @SerializedName("intelligence")
    private int intelligence;
    @SerializedName("perception")
    private int perception;
    @SerializedName("willpower")
    private int willpower;
    @SerializedName("fellowship")
    private int fellowship;
    @SerializedName("influence")
    private int influence;
    @SerializedName("corruption")
    private int corruption;
    @SerializedName("insanity")
    private int insanity;

    @SerializedName("homeworld")
    private String homeWorld;
    @SerializedName("background")
    private String background;
    @SerializedName("role")
    private String role;
    @SerializedName("elite_advances")
    private String eliteAdvances;
    @SerializedName("psy_level")
    private int psyRating;

    @SerializedName("skills")
    private List<SkillInfo> skillList = new ArrayList<SkillInfo>();
    @SerializedName("talents")
    private List<Integer> talentIds = new ArrayList<Integer>();
    @SerializedName("traits")
    private List<Integer> traitIds = new ArrayList<Integer>();
    @SerializedName("items")
    private List<Integer> itemIds = new ArrayList<Integer>();


    private Map<DH2ESkill,Integer> skillMap = new HashMap<DH2ESkill,Integer>();
    private Set<DH2ETrait> traitsSet = new HashSet<DH2ETrait>();
    private Set<DH2ETalent> talentsSet = new HashSet<DH2ETalent>();

    private List<DH2EWeapon> weapons = new ArrayList<DH2EWeapon>();
    private List<DH2EArmor> armors = new ArrayList<DH2EArmor>();
    private List<DH2EPowerField> powerFields = new ArrayList<DH2EPowerField>();
    private List<DH2EItem> gear = new ArrayList<DH2EItem>();

    public DH2ECharacter(String name,String playerName) {
        super(name, playerName);
    }

    @Override
    public String getSummary() {
        return "DH2E char";
    }

    public void initSkillsTraitsAndTalents(DH2ERules rules)
    {
        skillMap = new HashMap<DH2ESkill, Integer>();
        for (SkillInfo skillInfo:skillList)
        {
            DH2ESkill skill = rules.getSkill(skillInfo.skillId);
            skillMap.put(skill, skillInfo.level);
        }

        traitsSet = new HashSet<DH2ETrait>();
        for (int traitId:traitIds)
        {
            traitsSet.add(rules.getTrait(traitId));
        }

        talentsSet = new HashSet<DH2ETalent>();
        for (int talentId:talentIds)
        {
            talentsSet.add(rules.getTalent(talentId));
        }
    }

    public void initGear(DH2EItemsBundle itemBundle)
    {
        for (int id:itemIds)
        {
            if (itemBundle.getWeaponKeys().contains(id))
                weapons.add(itemBundle.getWeapon(id));

            if (itemBundle.getArmorKeys().contains(id))
                armors.add(itemBundle.getArmor(id));

            if (itemBundle.getPowerFieldKeys().contains(id))
                powerFields.add(itemBundle.getPowerField(id));

            if (itemBundle.getGearKeys().contains(id))
                gear.add(itemBundle.getGearItem(id));
        }
    }

    public Map<DH2ESkill, Integer> getSkillMap() {
        return new HashMap<DH2ESkill, Integer>(skillMap);
    }

    public Set<DH2ETrait> getTraits() {
        return new HashSet<DH2ETrait>(traitsSet);
    }

    public Set<DH2ETalent> getTalents() {
        return new HashSet<DH2ETalent>(talentsSet);
    }

    public List<DH2EWeapon> getWeapons() {
        return new ArrayList<DH2EWeapon>(weapons);
    }

    public List<DH2EArmor> getArmors() {
        return new ArrayList<DH2EArmor>(armors);
    }

    public List<DH2EPowerField> getPowerFields() {
        return new ArrayList<DH2EPowerField>(powerFields);
    }

    public List<DH2EItem> getGear() {
        return new ArrayList<DH2EItem>(gear);
    }

    public int getWeaponSkill() {
        return weaponSkill;
    }

    public int getBallisticSkill() {
        return ballisticSkill;
    }

    public int getStrength() {
        return strength;
    }

    public int getToughness() {
        return toughness;
    }

    public int getAgility() {
        return agility;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int getPerception() {
        return perception;
    }

    public int getWillpower() {
        return willpower;
    }

    public int getFellowship() {
        return fellowship;
    }

    public int getInfluence() {
        return influence;
    }

    public String getHomeWorld() {
        return homeWorld;
    }

    public String getBackground() {
        return background;
    }

    public String getRole() {
        return role;
    }

    public String getEliteAdvances() {
        return eliteAdvances;
    }

    public int getPsyRating() {
        return psyRating;
    }

    public int getInsanity() {
        return insanity;
    }

    public int getCorruption() {
        return corruption;
    }
}
