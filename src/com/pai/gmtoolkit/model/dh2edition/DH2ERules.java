package com.pai.gmtoolkit.model.dh2edition;

import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ESkill;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETalent;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETrait;
import com.pai.gmtoolkit.model.dh2edition.items.DH2EItemsBundle;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by 1 on 04.03.2015.
 */
public class DH2ERules {
    private Map<Integer,DH2ESkill> skills = new TreeMap<Integer,DH2ESkill>();
    private Map<Integer,DH2ETrait> traits = new TreeMap<Integer,DH2ETrait>();
    private Map<Integer,DH2ETalent> talents = new TreeMap<Integer,DH2ETalent>();
    private DH2EItemsBundle itemsBundle;

    public DH2ERules() {}

    public void addSkills(DH2ESkill[] skills) {
        for (DH2ESkill skill:skills)
        {
            this.skills.put(skill.getId(),skill);
        }
    }

    public DH2ESkill getSkill(int id)
    {
        return skills.get(id);
    }

    public DH2ETrait getTrait(int id)
    {
        return traits.get(id);
    }

    public void addTraits(DH2ETrait[] traits) {
        for (DH2ETrait trait:traits)
        {
            this.traits.put(trait.getId(),trait);
        }
    }

    public DH2ETalent getTalent(int id)
    {
        return talents.get(id);
    }

    public void addTalents(DH2ETalent[] talents) {
        for (DH2ETalent talent:talents)
        {
            this.talents.put(talent.getId(), talent);
        }
    }

    public void setItemsBundle(DH2EItemsBundle itemsBundle) {
        this.itemsBundle = itemsBundle;
    }
}
