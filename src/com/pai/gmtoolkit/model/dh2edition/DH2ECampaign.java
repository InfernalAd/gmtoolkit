package com.pai.gmtoolkit.model.dh2edition;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ESkill;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETalent;
import com.pai.gmtoolkit.model.dh2edition.char_params.DH2ETrait;
import com.pai.gmtoolkit.model.dh2edition.items.DH2EItemsBundle;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by 1 on 04.03.2015.
 */
public class DH2ECampaign {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("path")
    private String directoryPath;
    private boolean isDownloaded = false;
    private boolean isSelected = false;

    private DH2ECharactersBundle characters;
    private DH2ERules rules = new DH2ERules();



    private Context ctx;

    public static int selectedCampaignId = -1;
    public static DH2ECampaign selectedCampaign = null;

    public void loadFromJson()
    {
        try {
            Gson gson = new Gson();
            FileReader characterReader = new FileReader(new File(ctx.getExternalFilesDir(null) + "/" + directoryPath+"characters.json"));
            characters = gson.fromJson(characterReader, DH2ECharactersBundle.class);


            FileReader skillsReader = new FileReader(new File(ctx.getExternalFilesDir(null) + "/" + directoryPath+"skills.json"));
            DH2ESkill[] skills = gson.fromJson(skillsReader,DH2ESkill[].class);
            rules.addSkills(skills);

            FileReader traitsReader = new FileReader(new File(ctx.getExternalFilesDir(null) + "/" + directoryPath+"traits.json"));
            DH2ETrait[] traits = gson.fromJson(traitsReader,DH2ETrait[].class);
            rules.addTraits(traits);

            FileReader talentsReader = new FileReader(new File(ctx.getExternalFilesDir(null) + "/" + directoryPath+"talents.json"));
            DH2ETalent[] talents = gson.fromJson(talentsReader,DH2ETalent[].class);
            rules.addTalents(talents);

            FileReader itemsReader = new FileReader(new File(ctx.getExternalFilesDir(null) + "/" + directoryPath+"items.json"));

            DH2EItemsBundle itemsBundle = gson.fromJson(itemsReader, DH2EItemsBundle.class);
            rules.setItemsBundle(itemsBundle);
            itemsBundle.initHashMaps();

            for (DH2ECharacter playerCharacter:characters.getPlayerCharacters())
            {
                playerCharacter.initGear(itemsBundle);
                playerCharacter.initSkillsTraitsAndTalents(rules);
            }

            for (DH2ECharacter nonPlayerCharacters:characters.getPlayerCharacters())
            {
                nonPlayerCharacters.initGear(itemsBundle);
                nonPlayerCharacters.initSkillsTraitsAndTalents(rules);
            }
            Log.d("", "");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setDownloaded(boolean isDownloaded) {
        this.isDownloaded = isDownloaded;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public DH2ECharactersBundle getCharacters() {
        return characters;
    }

    public DH2ERules getRules() {
        return rules;
    }
}
