package com.pai.gmtoolkit.model.dh2edition.char_params;

import android.content.Context;
import com.google.gson.annotations.SerializedName;
import com.pai.gmtoolkit.R;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by 1 on 04.03.2015.
 */
public class DH2ESkill implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("characteristic")
    private String characteristic;

    public DH2ESkill(String name, String characteristic) {
        this.name = name;
        this.characteristic = characteristic;
    }

    public String getName() {
        return name;
    }

    public String getCharacteristic() {
        return characteristic;
    }

    public int getId() {
        return id;
    }
}
