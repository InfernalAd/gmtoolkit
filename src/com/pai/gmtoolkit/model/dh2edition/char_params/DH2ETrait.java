package com.pai.gmtoolkit.model.dh2edition.char_params;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 1 on 04.03.2015.
 */
public class DH2ETrait implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
